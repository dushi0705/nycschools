//
//  NYCSchoolsApp.swift
//  NYCSchools
//
//  Created by Dushyanth Challagundla on 1/25/24.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
