//
//  NetworkManager.swift
//  NYCSchools
//
//  Created by Dushyanth Challagundla on 1/25/24.
//

import Foundation


class NetworkManager {
    static let shared =  NetworkManager()
    private init() {
        
    }
    func fetchData(completion: @escaping (Result<[SchoolModel], Error>) -> Void) {
        guard let apiURL = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {
            //completion(.failure(err))
            return
        }
        let session = URLSession.shared
        let req = URLRequest(url: apiURL)
        let task = session.dataTask(with: apiURL) { data, resp, error in
            
            if let err = error {
                completion(.failure(err))
                return
            }
            guard let httpResp = resp as? HTTPURLResponse, (200...299).contains(httpResp.statusCode) else {
                let statusCode = (resp as? HTTPURLResponse)?.statusCode ?? -1
                let error = NSError(domain: "APIError", code: statusCode)
                completion(.failure(error))
                return
            }
            guard let respData = data else {
                let error = NSError(domain: "APIError", code: -1)
                completion(.failure(error))
                return
            }
            do {
                let schools = try JSONDecoder().decode([SchoolModel].self, from: respData)
                completion(.success(schools))
                
                return
            } catch let error {
                print(error)
                completion(.failure(error))
            }
        }
        task.resume()
        
    }
    
}
