//
//  SchoolsViewModel.swift
//  NYCSchools
//
//  Created by Dushyanth Challagundla on 1/25/24.
//

import Foundation


class SchoolsViewModel: ObservableObject {
    @Published var schools: [SchoolModel] = []
    
    func fetchData() {
        NetworkManager.shared.fetchData { [weak self]result in
            switch result {
            case .success(let schools):
                DispatchQueue.main.async {
                    self?.schools = schools
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.schools = []
                }
                
                print("Handle error case here")
            }
        }
    }
}
