//
//  SchoolModel.swift
//  NYCSchools
//
//  Created by Dushyanth Challagundla on 1/25/24.
//

import Foundation

struct SchoolModel : Decodable, Identifiable {
    var id: String? = UUID().uuidString
    var dbn: String?
    var school_name:String?
    var overview_paragraph:String?

    /*enum codingKeys: String, CodingKey {
        case id
        case dbn
        case schoolName = "school_name"
    }*/
}
